#ifndef NUT_SIMULATION_DATA_GENERATOR
#define NUT_SIMULATION_DATA_GENERATOR

#include <SimulationChannelDescriptor.h>
#include <string>
class NUTAnalyzerSettings;

class NUTSimulationDataGenerator
{
public:
	NUTSimulationDataGenerator();
	~NUTSimulationDataGenerator();

	void Initialize( U32 simulation_sample_rate, NUTAnalyzerSettings* settings );
	U32 GenerateSimulationData( U64 newest_sample_requested, U32 sample_rate, SimulationChannelDescriptor** simulation_channel );

protected:
	NUTAnalyzerSettings* mSettings;
	U32 mSimulationSampleRateHz;

protected:
	void CreateSerialByte();
	std::string mSerialText;
	U32 mStringIndex;

	SimulationChannelDescriptor mSerialSimulationData;

};
#endif //NUT_SIMULATION_DATA_GENERATOR