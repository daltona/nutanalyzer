#include "NUTAnalyzer.h"
#include "NUTAnalyzerSettings.h"
#include "AnalyzerChannelData.h"

NUTAnalyzer::NUTAnalyzer()
:	Analyzer2(),
	mSettings( new NUTAnalyzerSettings() ),
	mSimulationInitilized( false )
{
	SetAnalyzerSettings( mSettings.get() );
}

NUTAnalyzer::~NUTAnalyzer()
{
	KillThread();
}

void NUTAnalyzer::SetupResults()
{
	mResults.reset( new NUTAnalyzerResults( this, mSettings.get() ) );
	SetAnalyzerResults( mResults.get() );
	mResults->AddChannelBubblesWillAppearOn( mSettings->mPhy1Channel );
}


void NUTAnalyzer::Sync() {

	if (mSync->GetBitState() == BIT_LOW)
		mSync->AdvanceToNextEdge();
	mSync->AdvanceToNextEdge();

	mPhy1->AdvanceToAbsPosition(mSync->GetSampleNumber());

	if (mPhy1->GetBitState() == BIT_LOW)
		mPhy1->AdvanceToNextEdge();
}

void NUTAnalyzer::WorkerThread()
{
	mSampleRateHz = GetSampleRate();

	mPhy1 = GetAnalyzerChannelData(mSettings->mPhy1Channel);
	mPhy2 = GetAnalyzerChannelData(mSettings->mPhy2Channel);
	mSync = GetAnalyzerChannelData(mSettings->mSyncChannel);
	mIsa = GetAnalyzerChannelData(mSettings->mIsaChannel);
	mData = GetAnalyzerChannelData(mSettings->mDataChannel);

	Sync();

	U8 phase = 0;
	U16 isaValue = 0;
	U16 dataValue = 0;
	U64 isaStartPos = mPhy1->GetSampleNumber();
	U64 dataStartPos = 0;
	U64 cValue = 0;

	for( ; ; )
	{
		Frame dataFrame;
		U64 start = mPhy1->GetSampleNumber();
		mPhy1->AdvanceToNextEdge(); //falling edge -- beginning of the start bit

		if (phase > 45) {
			mIsa->AdvanceToAbsPosition(mPhy1->GetSampleNumber());
			if (mIsa->GetBitState() == 1) {
				isaValue |= 1 << (phase-46);
			}
		}

		if (phase > 15 && phase <= 31) {
			if (phase == 16) {
				dataStartPos = mPhy1->GetSampleNumber();
			}
			mIsa->AdvanceToAbsPosition(mPhy1->GetSampleNumber());
			if (mIsa->GetBitState() == 1) {
				dataValue |= 1 << (31 - phase);
			}
			if (phase == 31) {
				dataValue = ((dataValue << 8) & 0xff00) | ((dataValue >> 8) & 0xff);
				U8 lsb = 0;
				U8 msb = 0;
				lsb |= dataValue & 0x80 ? 1 : 0;
				lsb |= dataValue & 0x40 ? 0x2 : 0;
				lsb |= dataValue & 0x20 ? 0x4 : 0;
				lsb |= dataValue & 0x10 ? 0x8 : 0;
				lsb |= dataValue & 0x8 ? 0x10 : 0;
				lsb |= dataValue & 0x4 ? 0x20 : 0;
				lsb |= dataValue & 0x2 ? 0x40 : 0;
				lsb |= dataValue & 0x1 ? 0x80 : 0;

				msb |= dataValue & 0x8000 ? 1 : 0;
				msb |= dataValue & 0x4000 ? 0x2 : 0;
				msb |= dataValue & 0x2000 ? 0x4 : 0;
				msb |= dataValue & 0x1000 ? 0x8 : 0;
				msb |= dataValue & 0x800 ? 0x10 : 0;
				msb |= dataValue & 0x400 ? 0x20 : 0;
				msb |= dataValue & 0x200 ? 0x40 : 0;
				msb |= dataValue & 0x100 ? 0x80 : 0;
				dataValue = (msb << 8) | lsb;
			}
		}

		//we have a byte to save.
		Frame frame;
		mData->AdvanceToAbsPosition(mPhy1->GetSampleNumber());

		if (phase > 1) {
			cValue |= (mData->GetBitState() == 1) ? 1 << (phase-2) : 0;
		}
		frame.mData1 = phase++;

		if (phase == 56) {
			Frame isaFrame;
			phase = 0;
			mSync->AdvanceToAbsPosition(mPhy1->GetSampleNumber());
			isaFrame.mType = mSync->GetBitState() ? 1 : 3;
			isaFrame.mData1 = isaValue | dataValue << 16;
			isaFrame.mData2 = cValue;
			isaFrame.mFlags = 0;
			isaFrame.mStartingSampleInclusive = isaStartPos;
			isaFrame.mEndingSampleInclusive = mPhy1->GetSampleNumber();
			U64 fid = mResults->AddFrame(isaFrame);
			mResults->GenerateBubbleText(fid, mSettings->mIsaChannel, Hexadecimal);
			//mResults->GenerateFrameTabularText(fid, Hexadecimal);
			mResults->CommitResults();
			ReportProgress(isaFrame.mEndingSampleInclusive);
			isaValue = 0;
			dataValue = 0;
			cValue = 0;
		}
		U64 phyfall = mPhy1->GetSampleNumber();

		mPhy1->AdvanceToNextEdge(); //rising edge -- beginning of the start bit
#if 1
		if ((mPhy1->GetSampleNumber() - phyfall) > 1000) {
			mSync->AdvanceToAbsPosition(phyfall);
			Sync();
			isaValue = 0;
			dataValue = 0;
			cValue = 0;
			phase = 0;
			isaStartPos = mPhy1->GetSampleNumber();
			continue;
		}
#endif
		if (phase < 16) {
			frame.mFlags = 0;
			frame.mType = 2;
			frame.mStartingSampleInclusive = start;
			frame.mEndingSampleInclusive = mPhy1->GetSampleNumber();
			mResults->AddFrame( frame );
			mResults->CommitResults();
			ReportProgress( frame.mEndingSampleInclusive );
		}

		if (phase == 45) {
			Frame dataFrame;

			dataFrame.mData1 = dataValue;
			dataFrame.mFlags = 0;
			dataFrame.mType = 0;
			dataFrame.mStartingSampleInclusive = dataStartPos;
			dataFrame.mEndingSampleInclusive = mPhy1->GetSampleNumber();
			//U64 fid = mResults->AddFrame(dataFrame);
			//mResults->GenerateBubbleText(fid, mSettings->mDataChannel, Hexadecimal);
			//mResults->GenerateFrameTabularText(fid, Hexadecimal);
			//mResults->CommitResults();
			isaStartPos = mPhy1->GetSampleNumber();
			//ReportProgress(dataFrame.mEndingSampleInclusive);
		}

	}
}

bool NUTAnalyzer::NeedsRerun()
{
	return false;
}

U32 NUTAnalyzer::GenerateSimulationData( U64 minimum_sample_index, U32 device_sample_rate, SimulationChannelDescriptor** simulation_channels )
{
	if( mSimulationInitilized == false )
	{
		mSimulationDataGenerator.Initialize( GetSimulationSampleRate(), mSettings.get() );
		mSimulationInitilized = true;
	}

	return mSimulationDataGenerator.GenerateSimulationData( minimum_sample_index, device_sample_rate, simulation_channels );
}

U32 NUTAnalyzer::GetMinimumSampleRateHz()
{
	return 1000000;
}

const char* NUTAnalyzer::GetAnalyzerName() const
{
	return "NUT";
}

const char* GetAnalyzerName()
{
	return "NUT";
}

Analyzer* CreateAnalyzer()
{
	return new NUTAnalyzer();
}

void DestroyAnalyzer( Analyzer* analyzer )
{
	delete analyzer;
}
