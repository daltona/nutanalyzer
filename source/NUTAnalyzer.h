#ifndef NUT_ANALYZER_H
#define NUT_ANALYZER_H

#include "Analyzer.h"
#include "NUTAnalyzerResults.h"
#include "NUTSimulationDataGenerator.h"

class NUTAnalyzerSettings;
class ANALYZER_EXPORT NUTAnalyzer : public Analyzer2
{
	void Sync();

public:
	NUTAnalyzer();
	virtual ~NUTAnalyzer();

	virtual void SetupResults();
	virtual void WorkerThread();

	virtual U32 GenerateSimulationData( U64 newest_sample_requested, U32 sample_rate, SimulationChannelDescriptor** simulation_channels );
	virtual U32 GetMinimumSampleRateHz();

	virtual const char* GetAnalyzerName() const;
	virtual bool NeedsRerun();

protected: //vars
	std::auto_ptr< NUTAnalyzerSettings > mSettings;
	std::auto_ptr< NUTAnalyzerResults > mResults;
	AnalyzerChannelData* mPhy1;
	AnalyzerChannelData* mPhy2;
	AnalyzerChannelData* mSync;
	AnalyzerChannelData* mIsa;
	AnalyzerChannelData* mData;

	NUTSimulationDataGenerator mSimulationDataGenerator;
	bool mSimulationInitilized;

	//Serial analysis vars:
	U32 mSampleRateHz;
	U32 mStartOfStopBitOffset;
	U32 mEndOfStopBitOffset;
};

extern "C" ANALYZER_EXPORT const char* __cdecl GetAnalyzerName();
extern "C" ANALYZER_EXPORT Analyzer* __cdecl CreateAnalyzer( );
extern "C" ANALYZER_EXPORT void __cdecl DestroyAnalyzer( Analyzer* analyzer );

#endif //NUT_ANALYZER_H
