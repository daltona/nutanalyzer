#ifndef NUT_ANALYZER_SETTINGS
#define NUT_ANALYZER_SETTINGS

#include <AnalyzerSettings.h>
#include <AnalyzerTypes.h>

class NUTAnalyzerSettings : public AnalyzerSettings
{
public:
	NUTAnalyzerSettings();
	virtual ~NUTAnalyzerSettings();

	virtual bool SetSettingsFromInterfaces();
	void UpdateInterfacesFromSettings();
	virtual void LoadSettings( const char* settings );
	virtual const char* SaveSettings();

	
	Channel mPhy1Channel;
	Channel mPhy2Channel;
	Channel mSyncChannel;
	Channel mIsaChannel;
	Channel mDataChannel;

protected:
	std::auto_ptr< AnalyzerSettingInterfaceChannel >	mPhy1ChannelInterface;
	std::auto_ptr< AnalyzerSettingInterfaceChannel >	mPhy2ChannelInterface;
	std::auto_ptr< AnalyzerSettingInterfaceChannel >	mSyncChannelInterface;
	std::auto_ptr< AnalyzerSettingInterfaceChannel >	mIsaChannelInterface;
	std::auto_ptr< AnalyzerSettingInterfaceChannel >	mDataChannelInterface;
};

#endif //NUT_ANALYZER_SETTINGS
