#include "NUTSimulationDataGenerator.h"
#include "NUTAnalyzerSettings.h"

#include <AnalyzerHelpers.h>

NUTSimulationDataGenerator::NUTSimulationDataGenerator()
:	mSerialText( "My first analyzer, woo hoo!" ),
	mStringIndex( 0 )
{
}

NUTSimulationDataGenerator::~NUTSimulationDataGenerator()
{
}

void NUTSimulationDataGenerator::Initialize( U32 simulation_sample_rate, NUTAnalyzerSettings* settings )
{
	mSimulationSampleRateHz = simulation_sample_rate;
	mSettings = settings;

	mSerialSimulationData.SetChannel( mSettings->mPhy1Channel );
	mSerialSimulationData.SetInitialBitState( BIT_HIGH );
}

U32 NUTSimulationDataGenerator::GenerateSimulationData( U64 largest_sample_requested, U32 sample_rate, SimulationChannelDescriptor** simulation_channel )
{
	U64 adjusted_largest_sample_requested = AnalyzerHelpers::AdjustSimulationTargetSample( largest_sample_requested, sample_rate, mSimulationSampleRateHz );

	while( mSerialSimulationData.GetCurrentSampleNumber() < adjusted_largest_sample_requested )
	{
		CreateSerialByte();
	}

	*simulation_channel = &mSerialSimulationData;
	return 1;
}

void NUTSimulationDataGenerator::CreateSerialByte()
{

}
