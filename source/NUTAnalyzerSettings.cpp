#include "NUTAnalyzerSettings.h"
#include <AnalyzerHelpers.h>


NUTAnalyzerSettings::NUTAnalyzerSettings()
	: mPhy1Channel(UNDEFINED_CHANNEL),
	mPhy2Channel(UNDEFINED_CHANNEL),
	mSyncChannel(UNDEFINED_CHANNEL),
	mIsaChannel(UNDEFINED_CHANNEL),
	mDataChannel(UNDEFINED_CHANNEL)
{
	mPhy1ChannelInterface.reset(new AnalyzerSettingInterfaceChannel());
	mPhy1ChannelInterface->SetTitleAndTooltip("PHI1", "Standard NUT bus protocol decoder");
	mPhy1ChannelInterface->SetChannel(mPhy1Channel);

	mPhy2ChannelInterface.reset(new AnalyzerSettingInterfaceChannel());
	mPhy2ChannelInterface->SetTitleAndTooltip("PHI2", "Standard NUT bus protocol decoder");
	mPhy2ChannelInterface->SetChannel(mPhy2Channel);

	mSyncChannelInterface.reset(new AnalyzerSettingInterfaceChannel());
	mSyncChannelInterface->SetTitleAndTooltip("SYNC", "Standard NUT bus protocol decoder");
	mSyncChannelInterface->SetChannel(mSyncChannel);

	mIsaChannelInterface.reset(new AnalyzerSettingInterfaceChannel());
	mIsaChannelInterface->SetTitleAndTooltip("ISA", "Standard NUT bus protocol decoder");
	mIsaChannelInterface->SetChannel(mIsaChannel);

	mDataChannelInterface.reset(new AnalyzerSettingInterfaceChannel());
	mDataChannelInterface->SetTitleAndTooltip("DATA", "Standard NUT bus protocol decoder");
	mDataChannelInterface->SetChannel(mDataChannel);


	AddInterface(mPhy1ChannelInterface.get());
	AddInterface(mPhy2ChannelInterface.get());
	AddInterface(mSyncChannelInterface.get());
	AddInterface(mIsaChannelInterface.get());
	AddInterface(mDataChannelInterface.get());

	AddExportOption( 0, "Export as text/csv file" );
	AddExportExtension( 0, "text", "txt" );
	AddExportExtension( 0, "csv", "csv" );

	ClearChannels();
	AddChannel(mPhy1Channel, "PHI1", false);
	AddChannel(mPhy2Channel, "PHI2", false);
	AddChannel(mSyncChannel, "SYNC", false);
	AddChannel(mIsaChannel, "ISA", false);
	AddChannel(mDataChannel, "DATA", false);
}

NUTAnalyzerSettings::~NUTAnalyzerSettings()
{
}

bool NUTAnalyzerSettings::SetSettingsFromInterfaces()
{
	mPhy1Channel = mPhy1ChannelInterface->GetChannel();
	mPhy2Channel = mPhy2ChannelInterface->GetChannel();
	mSyncChannel = mSyncChannelInterface->GetChannel();
	mIsaChannel = mIsaChannelInterface->GetChannel();
	mDataChannel = mDataChannelInterface->GetChannel();

	ClearChannels();
	AddChannel(mPhy1Channel, "PHI1", true);
	AddChannel(mPhy2Channel, "PHI2", true);
	AddChannel(mSyncChannel, "SYNC", true);
	AddChannel(mIsaChannel, "ISA", true);
	AddChannel(mDataChannel, "DATA", true);

	return true;
}

void NUTAnalyzerSettings::UpdateInterfacesFromSettings()
{
	mPhy1ChannelInterface->SetChannel(mPhy1Channel);
	mPhy2ChannelInterface->SetChannel(mPhy2Channel);
	mSyncChannelInterface->SetChannel(mSyncChannel);
	mIsaChannelInterface->SetChannel(mIsaChannel);
	mDataChannelInterface->SetChannel(mDataChannel);
}

void NUTAnalyzerSettings::LoadSettings( const char* settings )
{
	SimpleArchive text_archive;
	text_archive.SetString( settings );

	text_archive >> mPhy1Channel;
	text_archive >> mPhy2Channel;
	text_archive >> mSyncChannel;
	text_archive >> mIsaChannel;
	text_archive >> mDataChannel;

	ClearChannels();
	
	AddChannel(mPhy1Channel, "PHI1", true);
	AddChannel(mPhy2Channel, "PHI2", true);
	AddChannel(mSyncChannel, "SYNC", true);
	AddChannel(mIsaChannel, "ISA", true);
	AddChannel(mDataChannel, "DATA", true);

	UpdateInterfacesFromSettings();
}

const char* NUTAnalyzerSettings::SaveSettings()
{
	SimpleArchive text_archive;
	text_archive << mPhy1Channel;
	text_archive << mPhy2Channel;
	text_archive << mSyncChannel;
	text_archive << mIsaChannel;
	text_archive << mDataChannel;

	return SetReturnString( text_archive.GetString() );
}
