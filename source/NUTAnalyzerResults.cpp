#include "NUTAnalyzerResults.h"
#include <AnalyzerHelpers.h>
#include "NUTAnalyzer.h"
#include "NUTAnalyzerSettings.h"
#include <iostream>
#include <fstream>

NUTAnalyzerResults::NUTAnalyzerResults( NUTAnalyzer* analyzer, NUTAnalyzerSettings* settings )
:	AnalyzerResults(),
	mSettings( settings ),
	mAnalyzer( analyzer )
{
}

NUTAnalyzerResults::~NUTAnalyzerResults()
{
}

void NUTAnalyzerResults::GenerateBubbleText( U64 frame_index, Channel& channel, DisplayBase display_base )
{
	ClearResultStrings();
	Frame frame = GetFrame( frame_index );
	U32 dl, dh;
	dh = frame.mData2 >> 32;
	dl = frame.mData2 & 0xffffffff;
	U16 data_addr = frame.mData2 >> 15 & 0x7fff;
	U16 c20 = frame.mData2 & 0x3ff;
	U16 c63 = frame.mData2 >> 12 & 0xffff;

	U16 addr, instr;
	addr = frame.mData1 >> 16;
	instr = frame.mData1 & 0xffff;

	char number_str[128];
	switch (frame.mType) {
	case 0:
		sprintf(number_str, "addr : %04x", frame.mData1);
		break;
		case 1:
			sprintf(number_str, "%04x I:%04x a:%04x d:%03x c:%08x%08x", addr, instr, c63, c20, dh, dl);
			break;
		case 3:
			sprintf(number_str, "%04x D:%04x a:%04x d:%03x c:%08x%08x", addr, instr, c63, c20, dh, dl);
			break;
	case 2:
		return;
	}
	AddResultString(number_str);
}

void NUTAnalyzerResults::GenerateExportFile( const char* file, DisplayBase display_base, U32 export_type_user_id )
{
	std::ofstream file_stream( file, std::ios::out );

	U64 trigger_sample = mAnalyzer->GetTriggerSample();
	U32 sample_rate = mAnalyzer->GetSampleRate();

	file_stream << "Time [s],Value" << std::endl;

	U64 num_frames = GetNumFrames();
	for( U32 i=0; i < num_frames; i++ )
	{
		Frame frame = GetFrame( i );

		char time_str[128];
		AnalyzerHelpers::GetTimeString( frame.mStartingSampleInclusive, trigger_sample, sample_rate, time_str, 128 );

		char number_str[128];
		AnalyzerHelpers::GetNumberString( frame.mData1, display_base, 8, number_str, 128 );

		file_stream << time_str << "," << number_str << std::endl;

		if( UpdateExportProgressAndCheckForCancel( i, num_frames ) == true )
		{
			file_stream.close();
			return;
		}
	}

	file_stream.close();
}

void NUTAnalyzerResults::GenerateFrameTabularText( U64 frame_index, DisplayBase display_base )
{
	Frame frame = GetFrame( frame_index );
	ClearTabularText();
	U32 dl, dh;
	dh = frame.mData2 >> 32;
	dl = frame.mData2 & 0xffffffff;

	U16 data_addr = frame.mData2 >> 15 & 0x7fff;
	U16 c20 = frame.mData2 & 0x3ff;
	U16 c63 = frame.mData2 >> 12 & 0xffff;

	U16 addr, instr;
	addr = frame.mData1 >> 16;
	instr = frame.mData1 & 0xffff;

	char number_str[128];
	switch (frame.mType) {
	case 0:
		sprintf(number_str, "addr : %04x", frame.mData1);
		break;
		case 1:
			sprintf(number_str, "%04x I:%04x a:%04x d:%03x c:%08x%08x", addr, instr, c63, c20, dh, dl);
			break;
		case 3:
			sprintf(number_str, "%04x D:%04x a:%04x d:%03x c:%08x%08x", addr, instr, c63, c20, dh, dl);
			break;
	case 2:
		return;
	}

	AddTabularText( number_str );
}

void NUTAnalyzerResults::GeneratePacketTabularText( U64 packet_id, DisplayBase display_base )
{
	//not supported

}

void NUTAnalyzerResults::GenerateTransactionTabularText( U64 transaction_id, DisplayBase display_base )
{
	//not supported
}
